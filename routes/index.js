var express = require('express');
var router = express.Router();

var bodyparser = require('body-parser');
var urlencodedparser = bodyparser.urlencoded({extended:false});
var qs = require('querystring');
var ejs = require('ejs');

// Load wink-pos-tagger.
var posTagger = require( 'wink-pos-tagger' );
 
// Create an instance of the pos tagger.
var tagger = posTagger();

const excludedWords = require('../public/javascripts/excludedWords');




//Geocoder for geonames.org
var GeocoderGeonames = require('geocoder-geonames');
var geocoder = new GeocoderGeonames({ username: 'julia94'});

var coord_x = [];
var coord_y = [];
var citynames = [];


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {page:'TOPOTEXT-web', menuId:'home'});
});

router.get('/about', function(req, res, next) {
  res.render('about', {page:'About this project', menuId:'about'});
});

router.get('/contact', function(req, res, next) {
  res.render('contact', {page:'Contact', menuId:'contact'});
});

router.get('/imprint', function(req, res, next) {
  res.render('imprint', {page:'Imprint and Disclaimer', menuId:'imprint'});
});


router.get('/process', function(req,res, next) {
  res.render('process', {page: 'Process Text to Map', menuId:'process', data: req.body});
  res.send(req.body);
});





router.post('/generateMap', urlencodedparser, function(req, res){
  //var obj = {};

  if (req.method === 'POST') {
    let body = '';
    req.on('data', chunk => {
        body += chunk.toString();
        //console.log(body);

        res.send(body);
    });
    req.on('end', () => {
        //set the querystring to lowercase
        body = body.toLowerCase();
        
        //console.log(body);        
        /*
        var post = body.split(' ');

        for (let n=0; n < excludedWords.length; n++) {
            var index = post.indexOf(excludedWords[n]);
            if (post.indexOf(excludedWords[n]) != -1) {
                post.splice(index, 1);
            }
        } */
        var post = tagger.tagSentence(body);
        //console.log(post);

        if (post.length > 0) {
            for (let i=0; i < post.length; i++) {
                if (post[i].pos == "NNP" || post[i].pos == "NN") {
                    geocoder.get('search', {
                        q: post[i].value
                    })
                    .then(function(response){
                       //console.log(post[i].value + ': ' + response.geonames[0].lat +  '/' + response.geonames[0].lng);
                      var latitude = [];
                      var longitude = [];
                      
                      
                      coord_x[i] = response.geonames[0].lat;
                      coord_y[i] = response.geonames[0].lng;
                      citynames[i] = response.geonames[0].toponymName;
                      /*coord_x.push(response.geonames[0].lat);
                      coord_y.push(response.geonames[0].lng);
                      citynames.push(response.geonames[0].toponymName); 
                      */
                      
    
                      //console.log(citynames);
                      //save results from response.geonames
                      var georesults = response.geonames;
                      if (response != '') {
                          for(let k=0; k < georesults.length; k++) {
                              latitude[k] = response.geonames[k].lat;
                              longitude[k] = response.geonames[k].lng;
                          }
                      }
                    })
                    .catch(function(error){
                        console.log(error);
                    });
                }
                  
            }
           
        }     
            
        
        res.end('ok'); 
    }); 
    
  } 
  
 
});


router.get('/showMap', function(req, res, next) {
    res.render('showMap', {page:'Show Map', menuId:'showMap', x: coord_x, y: coord_y, cnames: citynames});
});


module.exports = router;
