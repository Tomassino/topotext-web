
$(document).ready(function(){
    const $form = $("#textmapper");
    
    $("form#textmapper").on('submit', function(e){
        e.preventDefault();
        var daten = $('textarea[name=textbody]').val();  //console.log(JSON.stringify(daten)); 
        $.ajax({
            type: 'post',
            url: '/generateMap',
            data: daten,
            contentType: 'text',
            success: function(data) {
                console.log('success');
                const btn_Map = '<button class="btn"><i class="fas fa-globe"></i> <a href="/showMap" target="_blank">Show map</a></button>'; 
                //$('h1').html(data);
                $("#lnk").html(btn_Map);
            }
        })
        
    });
});
